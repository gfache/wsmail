package fr.afcepf.al33.api;

import fr.afcepf.al33.dto.InfoMailDTO;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import java.rmi.Remote;
import java.rmi.RemoteException;

@WebService(targetNamespace = "http://wsmail.afcepf.fr")
public interface IWSMail extends Remote {

    @WebMethod (operationName = "envoyerMail")
    @WebResult (name = "resultMail")
    boolean envoyerMail(@WebParam(name = "infoMailDTO")InfoMailDTO infoMailDTO) throws RemoteException;
}
